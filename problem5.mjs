const problem5 = (carYears) => {
  if (Array.isArray(carYears)) {
    let olderCars = carYears.filter((year) => {
      //filtering car years older than 2000
      return year < 2000;
    });
    return olderCars;
  } else {
    return;
  }
};

export default problem5;
