const problem6 = (inventory) => {
  if (Array.isArray(inventory)) {
    const bmwAndAudi = inventory.filter((car) => {
      //filtering only bmw and audi car using filter method
      return car.car_make == "Audi" || car.car_make == "BMW";
    });
    return bmwAndAudi;
  } else {
    return;
  }
};

export default problem6;
