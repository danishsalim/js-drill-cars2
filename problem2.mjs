const problem2 = (inventory) => {
  if (Array.isArray(inventory)) {
    return inventory[inventory.length - 1];
  } else {
    return null;
  }
};

export default problem2;
