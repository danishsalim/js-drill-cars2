const problem4 = (inventory) => {
  if (Array.isArray(inventory)) {
    let carYears = inventory.map((car) => {
      //map method iterate and return an array of car years
      return car.car_year;
    });
    return carYears;
  } else {
    return;
  }
};

export default problem4;
