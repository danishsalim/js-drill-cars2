const problem1 = (inventory, id) => {
  if (Array.isArray(inventory)) {
    let desiredCar = inventory.filter((car) => {
      //filter method is used to filtering cars with carid==id
      return car.id == id;
    });
    return desiredCar[0];
  } else {
    return;
  }
};

export default problem1;
