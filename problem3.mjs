const problem3 = (inventory) => {
  if (Array.isArray(inventory)) {
    let carModels = inventory //here map method return only the car model in an array
      .map((car) => {
        return car.car_model;
      })
      .sort(); //sort method sort the car model alphabetically
    return carModels;
  } else {
    return;
  }
};

export default problem3;
